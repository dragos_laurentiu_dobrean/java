package persistance;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import models.Advertisement;
import models.CarOption;
import models.Manufacturer;
import models.Model;
import models.User;

public class AdvertisementsPersistance {

	private static final String kAllOptions = "Select a from CarOption a";
	private static final String kAllManufacturers = "Select a from Manufacturer a";
	private static final String kAllAdverts = "Select a from Advertisement a";
	private static final String kAllAdvertsFilter = "Select a from Advertisement a where 1=1";
	
	public EntityManagerHelper managerHelper;
	
	public AdvertisementsPersistance() {
		this.managerHelper = new EntityManagerHelper();
	}
	
	public void addOption(CarOption o) {
		
		try {
			EntityManager em = this.managerHelper.getPreparedEntityManager();
			em.persist(o);
			this.managerHelper.commitAndClose(em);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	public List<CarOption> getAllOptions() {
		try {
			EntityManager em = this.managerHelper.getPreparedEntityManager();
			
			Query query = em.createQuery(kAllOptions);

			List<CarOption> options = query.getResultList();
			
			return options;
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public void deleteOption(CarOption option) {
		try {
			EntityManager em = this.managerHelper.getEntityManager();
			
			CarOption o = em.find(CarOption.class, option.getOptionID());
			
			em.getTransaction().begin();
			em.remove(o);
			this.managerHelper.commitAndClose(em);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	
	public void addModel(Model m) {
		try {
			EntityManager em = this.managerHelper.getPreparedEntityManager();
			em.persist(m);
			this.managerHelper.commitAndClose(em);
		} catch (Exception e) {
			// do nothing
		}
	}
		
	public void deleteModel(Model m) {
		try {
			EntityManager em = this.managerHelper.getEntityManager();
			
			Model o = em.find(Model.class, m.getModelID());
			
			em.getTransaction().begin();
			em.remove(o);
			this.managerHelper.commitAndClose(em);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	public void addManufacturer(Manufacturer manufacturer) {
		try {
			
			for (Model m: manufacturer.getModels()) {
				this.addModel(m);
			}
			
			EntityManager em = this.managerHelper.getPreparedEntityManager();
			em.persist(manufacturer);
			this.managerHelper.commitAndClose(em);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	public List<Manufacturer> getAllManufacturers() {
		try {
			EntityManager em = this.managerHelper.getEntityManager();
			
			Query query = em.createQuery(kAllManufacturers);

			List<Manufacturer> options = query.getResultList();
			
			return options;
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public List<Model> getAllModelsForManufacturerID(int id) {
		try {
			EntityManager em = this.managerHelper.getEntityManager();
			
			Manufacturer man = em.find(Manufacturer.class, id);
					
			return man.getModels();
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public void deleteManufacturer(int manufacturerID) {
		try {
			EntityManager em = this.managerHelper.getEntityManager();
			
			Manufacturer o = em.find(Manufacturer.class, manufacturerID);
			
			em.getTransaction().begin();
			em.remove(o);
			this.managerHelper.commitAndClose(em);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	public void addModelForManufacturer(int manufacturerID, Model model) {
		try {
			this.addModel(model);
			
			EntityManager em = this.managerHelper.getEntityManager();
			
			Manufacturer o = em.find(Manufacturer.class, manufacturerID);
			
			em.getTransaction().begin();
			o.getModels().add(model);
			this.managerHelper.commitAndClose(em);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	public void removeModelForManufacturer(int manufacturerID, Model model) {
		try {			
			EntityManager em = this.managerHelper.getEntityManager();
			
			Manufacturer o = em.find(Manufacturer.class, manufacturerID);
			
			int indexToRemove = -1;
			for (int i = 0; i < o.getModels().size(); i++) {
				Model m = o.getModels().get(i);
				if (m.getName().equals(model.getName())) {
					indexToRemove = i;
					break;
				}
			}
			
			if (indexToRemove != -1) {
				em.getTransaction().begin();
				
				Model m = o.getModels().get(indexToRemove);
				o.getModels().remove(indexToRemove);
				em.getTransaction().commit();
				
				this.deleteModel(m);
			}
			
		} catch (Exception e) {
			// do nothing
		}
	}
	
	public void addAdvertisement(Advertisement a) {

//		try {
			EntityManager em = this.managerHelper.getPreparedEntityManager();
			em.persist(a);
			em.flush();
			this.managerHelper.commitAndClose(em);
//		} catch (Exception e) {
			// do nothing
		
//		}
	}
	
	public void deleteAdvertisement(Advertisement a) {
		try {
			EntityManager em = this.managerHelper.getEntityManager();
			
			Advertisement o = em.find(Advertisement.class, a.getAdvertisementID());
			
			em.getTransaction().begin();
			em.remove(o);
			this.managerHelper.commitAndClose(em);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	public List<Advertisement> getAllAdvertisments() {
		try {
			EntityManager em = this.managerHelper.getPreparedEntityManager();
			
			Query query = em.createQuery(kAllAdverts);

			List<Advertisement> adverts = query.getResultList();
			
			return adverts;
			
		} catch (Exception e) {
			return null;
		}
	}
	
	public void addAdvertisementForUser(int userID, Advertisement advert) {
		try {
			this.addAdvertisement(advert);
			
			EntityManager em = this.managerHelper.getEntityManager();
			
			User o = em.find(User.class, userID);
			
			em.getTransaction().begin();
			o.getAdvertisements().add(advert);
			this.managerHelper.commitAndClose(em);
		} catch (Exception e) {
			// do nothing
		}
	}
	
	public void removeAdvertisementForUser(int userID, int advertisementID) {
		try {			
			EntityManager em = this.managerHelper.getEntityManager();
			
			User o = em.find(User.class, userID);
			
			int indexToRemove = -1;
			for (int i = 0; i < o.getAdvertisements().size(); i++) {
				Advertisement m = o.getAdvertisements().get(i);
				if (advertisementID == m.getAdvertisementID()) {
					indexToRemove = i;
					break;
				}
			}
			
			if (indexToRemove != -1) {
				em.getTransaction().begin();
				
				Advertisement m = o.getAdvertisements().get(indexToRemove);
				o.getAdvertisements().remove(indexToRemove);
				em.getTransaction().commit();
				
				this.deleteAdvertisement(m);
			}
			
		} catch (Exception e) {
			// do nothing
		}
	}
	
	public List<Advertisement> filterAdvertisements(Map<String, String> options, boolean ordered, String orderColumn, boolean asc) {
		
//		try {
			
			String queryString = kAllAdvertsFilter;
			
			for (String key : options.keySet()) {
				String value = options.get(key);
				
				queryString = queryString + " and a."+key+"='"+value+"' ";
			}
			
			if (ordered) {
				queryString = queryString + " ORDER BY a."+orderColumn;
				
				if (asc) {
					queryString = queryString + " ASC";
				} else {
					queryString = queryString + " DESC";
				}
				
			}
			
			EntityManager em = this.managerHelper.getPreparedEntityManager();
			
			Query query = em.createQuery(queryString);

			List<Advertisement> adverts = query.getResultList();
			
			return adverts;
			
//		} catch (Exception e) {
//			return null;
//		}
		
	}
	
	public static void main(String args[]) {
		System.out.println("adsa");
		
		AdvertisementsPersistance ap = new AdvertisementsPersistance();
		
		Model m = new Model("2");
		Model m2 = new Model("3");
		
		Manufacturer man = new Manufacturer("bmw");
		man.getModels().add(m);
		man.getModels().add(m2);
		
		Manufacturer man2 = new Manufacturer("audi");
		Model m3 = new Model("a3");
		Model m4 = new Model("a6");
		
		man2.getModels().add(m3);
		man2.getModels().add(m4);
		
//		ap.addManufacturer(man2);
		
//		Model m3 = new Model("sERIA 6");
//		ap.removeModelForManufacturer(2, m3);
		
//		List<Manufacturer> manufacturers = ap.getAllManufacturers();
//		ap.deleteManufacturer(1);
		
//		CarOption op = new CarOption("jeg");
//		ap.addOption(op);
//		Date utilDate = new java.util.Date();
////		
//		Advertisement adver = new Advertisement(manufacturers.get(1),manufacturers.get(1).getModels().get(1) , new Float(8888.34), 
//				"description la masina 2", ap.getAllOptions(), new java.sql.Date(utilDate.getTime()));
//		ap.removeAdvertisementForUser(1, 1);
		
//		ap.addAdvertisement(adver);
		
//		ap.addAdvertisementForUser(1, adver);
		
//		for (Advertisement adv : ap.getAllAdvertisments()) {
//			System.out.println("Yey:"+adv.getDescription());
////			ap.deleteAdvertisement(adv);
//		}
//		
//		
//		for (Manufacturer ma : manufacturers) {
//			System.out.println("Name: "+ma.getName());
//			
//			for (Model mo: ap.getAllModelsForManufacturerID(ma.getManufacturer_id())) {
//				System.out.println("Model:"+mo.getName());
//			}
//		}
		
		
		Map<String, String> filters = new HashMap<String, String>();
		filters.put("description", "description2");
		
		List<Advertisement> values = ap.filterAdvertisements(filters, false, "", false);
		
		for (Advertisement adv : values) {
		System.out.println("Yey:"+adv.getDescription());
//		ap.deleteAdvertisement(adv);
	}
		
	}
	
}
