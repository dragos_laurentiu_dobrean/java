package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.Admin;
import models.User;
import persistance.LoginPersistance;
import utils.AuthenticationHelper;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
        getServletContext().getRequestDispatcher("/WEB-INF/LoginPage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//if user is logged in take him directly to the main page
		if (AuthenticationHelper.isLoggedIn(request)) {
			response.sendRedirect("/mainPage");
		}
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		//if no username or password is provided show an login again with an error message
		if (username.isEmpty() || password.isEmpty()) {
		    request.setAttribute("invalid_message", "Please provide a usename and a password");
            getServletContext().getRequestDispatcher("/WEB-INF/LoginPage.jsp").forward(request, response);
            return;
		}
		
		LoginPersistance loginPersistance = new LoginPersistance();
		Admin admin = loginPersistance.getAdminByNameAndPass(username, password);
		User user = loginPersistance.getUserByNameAndPass(username, password);
		
		//check if the given credentials are for admin or regular user
		if (admin != null) {
			HttpSession ses = request.getSession(true);
			ses.setAttribute(AuthenticationHelper.KEY_USER_TYPE, "admin");
			
			getServletContext().getRequestDispatcher("/mainPage").forward(request, response);
		} else if (user != null) {
			HttpSession ses = request.getSession(true);
			ses.setAttribute(AuthenticationHelper.KEY_USER_TYPE, "user");
			response.sendRedirect("/mainPage");
		} else {
		    request.setAttribute("invalid_message", "Invalid username or password");
            getServletContext().getRequestDispatcher("/WEB-INF/LoginPage.jsp").forward(request, response);
		}
		
	}

}
