package controllers;

import java.util.List;
import java.util.Map;

import models.Advertisement;
import models.CarOption;
import models.Manufacturer;
import models.Model;
import persistance.AdvertisementsPersistance;

public class AdvertisementesController {
	private AdvertisementsPersistance persistance;
	
	public AdvertisementesController() {
		this.persistance = new AdvertisementsPersistance();
	}
	
	public void addCarOption(CarOption opt) {
		this.persistance.addOption(opt);
	}
	
	public void delCarOption(CarOption opt) {
		this.persistance.deleteOption(opt);
	}
	
	public void addModel(Model m) {
		this.persistance.addModel(m);
	}
	
	public void delModel(Model m) {
		this.persistance.deleteModel(m);
	}
	
	public void addManufacturer(Manufacturer m) {
		this.persistance.addManufacturer(m);
	}
	
	public void delManufacturer(int manufacturerID) {
		this.persistance.deleteManufacturer(manufacturerID);
	}
	
	public List<Manufacturer> getAllManufacturers() {
		return this.persistance.getAllManufacturers();
	}
	
	public List<CarOption> getAllOptions() {
		return this.persistance.getAllOptions();
	}
	
	public List<Model> getAllModelsForManufacture(int manufacturerID) {
		return this.persistance.getAllModelsForManufacturerID(manufacturerID);
	}
	
	public void addModelForManufacturer(int manufacturerID, Model m) {
		this.persistance.addModelForManufacturer(manufacturerID, m);
	}
	
	public void delModelForManufacturer(int manufacturerID, Model m) {
		this.persistance.removeModelForManufacturer(manufacturerID, m);
	}
	
	public void addAdvertisement(Advertisement a) {
		this.persistance.addAdvertisement(a);
	}
	
	public void removeAdvertisement(Advertisement a) {
		this.persistance.deleteAdvertisement(a);
	}
	
	public void addAdvertisementForUser(int userID, Advertisement a) {
		this.persistance.addAdvertisementForUser(userID, a);
	}
	
	public void delAdvertisementForUser(int userID, int advertID) {
		this.persistance.removeAdvertisementForUser(userID, advertID);
	}
	
	public List<Advertisement> filterAdvertisemenetsBy(Map<String, String> stringFilters, Map<String, Integer> numericalFilters, 
			boolean ordered, String columnForOrdering, boolean asc) {
		return this.persistance.filterAdvertisements(stringFilters, numericalFilters, null, ordered, columnForOrdering, asc, 0, 0);
	}
	
	public List<Advertisement> getAllAdverts() {
		return this.persistance.getAllAdvertisments();
	}
	
}
