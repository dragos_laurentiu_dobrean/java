package controllers;

import models.Admin;
import models.User;
import persistance.LoginPersistance;

public class LoginController {
	private LoginPersistance persistance;
	
	public LoginController() {
		this.persistance = new LoginPersistance();
	}
	
	public Admin addAdmin(Admin admin) {
		return this.persistance.addAdmin(admin);
	}
	
	public User addUser(User user) {
		return this.persistance.addUser(user);
	}
	
	// check if the user with user and pass exists
	// return null if it doesn't
	public User existsUser(User user) {
		return this.persistance.getUserByNameAndPass(user.getName(), user.getPass());
	}
	
	// check if the admin with user and pass exists
	// null if it doesn't
	public Admin existsAdmin(Admin admin) {
		return this.persistance.getAdminByNameAndPass(admin.getName(), admin.getPass());
	}
		
}
