package utils;

import javax.servlet.http.HttpServletRequest;


public class AuthenticationHelper {

	public static final String KEY_AUTHENTICATION_TOKEN = "authentication_token";
	public static final String KEY_USER_TYPE = "user_type";
	
	public static boolean isLoggedIn(HttpServletRequest request) {
		if (request.getSession(false) == null) {
			System.out.println("null session");
			return false;
		}
		
		if (request.getSession(false).getAttribute(KEY_AUTHENTICATION_TOKEN) == null) {
			System.out.println("null session: " + request.getSession().getAttribute(KEY_AUTHENTICATION_TOKEN));
			return false;
		}
		
		
		
		return true;
	}
	
	public static boolean isAdmin(HttpServletRequest request) {
		if (request.getSession(false) == null) {
			return false;
		}
		
		if (request.getSession(false).getAttribute(KEY_USER_TYPE).equals("admin")) {
			return true;
		}
		
		return false;
	}
	
}
