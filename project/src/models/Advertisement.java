package models;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * Entity implementation class for Entity: Advertisement
 *
 */
@Entity
public class Advertisement implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int advertisementID;
	
	 @ManyToOne(fetch=FetchType.LAZY)
	 @JoinColumn(name="MANUFACTURERID")
	private Manufacturer manufacturer;
	
	 @ManyToOne(fetch=FetchType.LAZY)
	 @JoinColumn(name="MODELID")
	private Model model;
	
	@OneToMany( targetEntity=CarOption.class)
    private List<CarOption> options;
	
	@ManyToOne
	public User user;
	
	@Lob
	private byte[] image;

	private Float price;
	private Date date;
	private String description;
	private String phone;

	public Advertisement() {
		super();
	}
	
	public Advertisement(Manufacturer manufacturer, Model model, Float price,String description, List<CarOption> options, Date date) {
		this.manufacturer = manufacturer;
		this.model = model;
		this.price = price;
		this.description = description;
		this.options = options;
		this.date = date;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getAdvertisementID() {
		return advertisementID;
	}

	public List<CarOption> getOptions() {
		if (options == null) {
			options = new ArrayList<CarOption>();
		}
		
		return options;
	}

	public void setOptions(List<CarOption> options) {
		this.options = options;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
	
	public void setAdvertisementID(int advertisementID) {
		this.advertisementID = advertisementID;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
