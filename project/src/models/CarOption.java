package models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * Entity implementation class for Entity: Option
 *
 */
@Entity
public class CarOption implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int optionID;
	
	@Column(unique=true)
	private String name;

	public CarOption() {
		super();
	}
	
	@Override
	public boolean equals(Object obj) {
		System.out.println("COmapre");
		if (obj instanceof Integer) {
			int i = (int)obj;
			System.out.println("Compare: " + i);
			return this.optionID == i;
		}
		CarOption opt = (CarOption) obj;
		return this.optionID == opt.getOptionID();
	}
	
	public CarOption(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getOptionID() {
		return optionID;
	}

	public void setOptionID(int optionID) {
		this.optionID = optionID;
	}
	
}
