package persistance;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import models.User;

public class Persistance {

	public static void main(String args[]) {
		System.out.println("hehe");
		
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory( "javaProject" );
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		
		User u = new User("hello", "bitch", "nana");
		
		entityManager.persist(u);
		
		entityManager.getTransaction().commit();
		entityManager.close();
	}
	
}
