package persistance;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import models.Advertisement;
import models.CarOption;
import models.Manufacturer;
import models.Model;

public class PersistanceTest {

	public static void populateDatabase() {
		
		AdvertisementsPersistance persistance = new AdvertisementsPersistance();
		
		Advertisement ad = new Advertisement();
		ad.setDate(new Date(100));
		ad.setDescription("First ad");
		
		Manufacturer man = new Manufacturer();
		man.setName("Audi");
		persistance.addManufacturer(man);
		man = persistance.getAllManufacturers().get(0);
		
		Model model = new Model();
		model.setName("A4");
		persistance.addModelForManufacturer(man.getManufacturer_id(), model);
		model = persistance.getAllModelsForManufacturerID(man.getManufacturer_id()).get(0);
		
		CarOption option1 = new CarOption();
		option1.setName("heated mirrors");
		CarOption option2 = new CarOption();
		option2.setName("navigation");
		CarOption option3 = new CarOption();
		option3.setName("leather seats");
		
		persistance.addOption(option1);
		persistance.addOption(option2);
		persistance.addOption(option3);
		
		List<CarOption> options = persistance.getAllOptions();
		
		ad.setManufacturer(man);
		ad.setModel(model);
		ad.setOptions(options);
		ad.setPrice(new Float(1200));
		
		persistance.addAdvertisement(ad);
		
	}
	
	public static void main(String[] args) {
		PersistanceTest.populateDatabase();
	}
	
}
