package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Advertisement;
import persistance.AdvertisementsPersistance;
import utils.AuthenticationHelper;

/**
 * Servlet implementation class RemoveServlet
 */
@WebServlet("/removeServlet")
public class RemoveServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RemoveServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (!AuthenticationHelper.isLoggedIn(request)) {
			getServletContext().getRequestDispatcher("/WEB-INF/NiceTry.jsp").forward(request, response);
		}
		
		String adId = request.getParameter("ad_id");
		if (adId == null || adId.isEmpty()) {
			response.sendRedirect("mainPage");
            return;
		}
		
		int id = Integer.parseInt(adId);
		Advertisement ad = new Advertisement();
		ad.setAdvertisementID(id);
		AdvertisementsPersistance persistance = new AdvertisementsPersistance();
		persistance.deleteAdvertisement(ad);
		
		response.sendRedirect("mainPage");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
