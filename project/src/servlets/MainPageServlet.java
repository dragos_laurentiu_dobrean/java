package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Advertisement;
import models.CarOption;
import models.Manufacturer;
import persistance.AdvertisementsPersistance;
import utils.AuthenticationHelper;

/**
 * Servlet implementation class MainPageServlet
 */
@WebServlet("/mainPage")
public class MainPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MainPageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String addButtonVisibilityProperty = "display : none;";
		String deleteButtonVisibilityProperty = "display : none;";
		
		if (AuthenticationHelper.isLoggedIn(request)) {
			addButtonVisibilityProperty = "display : initial;";
			
			if (AuthenticationHelper.isAdmin(request)) {
				deleteButtonVisibilityProperty = "display : initial;";
			}
		}
		
		AdvertisementsPersistance persistance = new AdvertisementsPersistance();
		List<Advertisement> ads = persistance.getAllAdvertisments();
		List<Manufacturer> manufacturer = persistance.getAllManufacturers();
		List<CarOption> options = persistance.getAllOptions();
		
		ads = filterBasedOnParameters(ads, request);
		
	    request.setAttribute("ads", ads);
	    request.setAttribute("manufacturer", manufacturer);
	    request.setAttribute("options", options);
	    
	    request.setAttribute("addVisibility", addButtonVisibilityProperty);
	    request.setAttribute("deleteVisibility", deleteButtonVisibilityProperty);
        getServletContext().getRequestDispatcher("/WEB-INF/MainPage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	private List<Advertisement> filterBasedOnParameters(List<Advertisement> ads, HttpServletRequest request) {
		String manuId = request.getParameter("manufacturer_id");
		String modId = request.getParameter("model_id");
		String sortBy = request.getParameter("sort_by");
		String sortOrder = request.getParameter("sort_order");
		String options[] = request.getParameterValues("car_options");
		String priceStart = request.getParameter("price_start");
		String priceEnd = request.getParameter("price_end");
		
		System.out.println(sortBy + " " + sortOrder);
		
		Map<String, Integer> map = new HashMap<>();
		boolean ordered = false;
		boolean orderAsc = true;
		String orderColumn = "";
		int priceS = -1;
		int priceE = -1;
		
		if (manuId != null && !manuId.equals("") && !manuId.equals("-2")) {
			int manufacturerId = Integer.parseInt(manuId);
			map.put("manufacturer.manufacturerID", manufacturerId);
		}
		
		if (modId != null && !modId.equals("") && !modId.equals("-2")) {
			int modelId = Integer.parseInt(modId);
			map.put("model.modelID", modelId);
		}
		
		List<Integer> optionsList = new ArrayList<>();
		if (options != null) {
			for (String o: options) {
				optionsList.add(Integer.parseInt(o));
			}
		}
		
		if (sortBy != null) {
			if (sortBy.equals("price")) {
				orderColumn = "price";
				ordered = true;
			} else if (sortBy.equals("date")) {
				orderColumn = "date";
				ordered = true;
			} else if (sortBy.equals("none")) {
				orderColumn = "";
			}
		}
		
		if (sortOrder != null) {
			if (sortOrder.equals("ascending")) {
				orderAsc = true;
			} else if (sortOrder.equals("descending")) {
				orderAsc = false;
			}
		}
		
		if (priceStart != null && !priceStart.equals("") && priceEnd != null && !priceEnd.equals("")) {
			priceS = Integer.parseInt(priceStart);
			priceE = Integer.parseInt(priceEnd);
		}
		
		AdvertisementsPersistance persistance = new AdvertisementsPersistance();
		List<Advertisement> filteredList = persistance.filterAdvertisements(null, map, optionsList, ordered, orderColumn, orderAsc, priceS, priceE);
		return filteredList;
		
	}

}
