package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.Admin;
import models.User;
import persistance.LoginPersistance;
import utils.AuthenticationHelper;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
        getServletContext().getRequestDispatcher("/WEB-INF/LoginPage.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.getSession(false).invalidate();
		System.out.println("Offf256");
		//if user is logged in take him directly to the main page
		if (AuthenticationHelper.isLoggedIn(request)) {
			System.out.println("Offf");
			response.sendRedirect("mainPage");
			return;
		}
		
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		//if no username or password is provided show an login again with an error message
		if (username.isEmpty() || password.isEmpty()) {

			System.out.println("Offf267");
		    request.setAttribute("invalid_message", "Please provide a usename and a password");
            getServletContext().getRequestDispatcher("/WEB-INF/LoginPage.jsp").forward(request, response);
            return;
		}
		
		LoginPersistance loginPersistance = new LoginPersistance();
		Admin admin = loginPersistance.getAdminByNameAndPass(username, password);
		User user = loginPersistance.getUserByNameAndPass(username, password);
		
		//check if the given credentials are for admin or regular user
		if (admin != null) {
			System.out.println("Offf1");
			HttpSession ses = request.getSession(true);
			ses.setAttribute(AuthenticationHelper.KEY_USER_TYPE, "admin");
			ses.setAttribute(AuthenticationHelper.KEY_AUTHENTICATION_TOKEN, "admin");
			response.sendRedirect("mainPage");
		} else if (user != null) {
			System.out.println("Offf2");
			HttpSession ses = request.getSession(true);
			ses.setAttribute(AuthenticationHelper.KEY_USER_TYPE, "user");
			ses.setAttribute(AuthenticationHelper.KEY_AUTHENTICATION_TOKEN, "user");
			response.sendRedirect("mainPage");
		} else {
			System.out.println("Offf3");
		    request.setAttribute("invalid_message", "Invalid username or password");
            getServletContext().getRequestDispatcher("/WEB-INF/LoginPage.jsp").forward(request, response);
		}
		
	}

}
