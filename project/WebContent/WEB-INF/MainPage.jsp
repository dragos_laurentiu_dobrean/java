<%@page import="models.Model"%>
<%@page import="models.Manufacturer"%>
<%@page import="models.CarOption"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="java.util.*" %>
<%@page import="models.Advertisement" %>
<%@page import="javax.xml.bind.DatatypeConverter" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
<title>Insert title here</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">



<style>
#content {
	background-color: #f0f0f4;
	border:1px solid #d3d3df;
	width: 50%;
	height: 100;
	margin: 0px 0px 0px 25%;
	padding-top: 20px;
    padding-right: 30px;
    padding-bottom: 50px;
    padding-left: 30px;
}

table {
    border-collapse: collapse;
    margin: 0px auto;  
    margin-bottom: 60px;          
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #4CAF50;
    color: white;
}

img {
	display: block;
    max-height:400px;
    width: auto;
    height: auto;
	margin: auto;
	margin-bottom: 10px;
}

ul
{
list-style-type: none;
}

.button
{
width   : 80px;
background  : none;
cursor  : pointer;
font-family : 'voltaeftu-regular',Times New Roman;
font-size   : 16px;
color   : #fff;
border  : 1px solid #0f0;
margin      : 0px;
padding     : 0px;
}

input {
    border: 0;
}


button {
    background-color: #3366ff;
    border: none;
    color: white;
    padding: 5px 10px;
    text-align: center;
    margin-top: 10px;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    width: 50%;
}

select {
	width: 50%;
}

.txtVal{
	width: 50%;
}

.lii{
    margin: 0;
    padding: 0;
	margin-top: 5px;

}

</style>



<script>

var previouslySelectedID = -1;

function hide() {
	if (previouslySelectedID != -1) {
		var selectV = document.getElementById(previouslySelectedID
				.toString());
		selectV.style.display = 'none';
		selectV.className = "";
	}
}

function show(tag) {
	previouslySelectedID = tag;
	var selectV = document.getElementById(tag.toString());
	selectV.style.display = 'inline';
	selectV.className = "visibleModel";
}

function manufacturerChanged(select) {
	hide();
	
	show(select.value);	
}

function addAdvertiesment() {
	alert("Adaugati mai hai hai");
}

function filter() {
	var manufacturerSelectTag = document.getElementById("manufacturer_select");
	var manufacturerId = manufacturerSelectTag.options[manufacturerSelectTag.selectedIndex].value;

	var modelSelectTag = document.getElementsByClassName("visibleModel")[0];
	if (!modelSelectTag) {
		// your code here.
	} else {
		var modelId = modelSelectTag.options[modelSelectTag.selectedIndex].value;
	}
		var sortBySelectTag = document.getElementById("sort_by");
		var sortBy = sortBySelectTag.options[sortBySelectTag.selectedIndex].value;

		var sortOrderSelectTag = document.getElementById("sort_order");
		var sortOrder = sortOrderSelectTag.options[sortOrderSelectTag.selectedIndex].value;
	
		var input = $("<input>")
    		.attr("type", "hidden")
    		.attr("name", "manufacturer_id").val(manufacturerId);
		var from = $('#easy_test').append($(input));
		
		input = $("<input>")
		.attr("type", "hidden")
		.attr("name", "model_id").val(modelId);
		from = $('#easy_test').append($(input));
		
		input = $("<input>")
		.attr("type", "hidden")
		.attr("name", "sort_by").val(sortBy);
		from = $('#easy_test').append($(input));
		
		input = $("<input>")
		.attr("type", "hidden")
		.attr("name", "sort_order").val(sortOrder);
		from = $('#easy_test').append($(input));
		
		from.submit();
}

</script>
</head>
<body>
<div id="content" >    
    
	<a href="addAdvertisement" style="<%= request.getAttribute("addVisibility") %>">Add advertisement</a>
	<% if (!request.getAttribute("addVisibility").equals("display : none;")) { %>
		<a style="float:right; clear:both; <%= request.getAttribute("addVisibility") %>" href="logoutServlet">Logout</a>
	<% } else { %>
		<a style="float:right; clear:both;" href="login">Login</a>
	<% } %>
	<p><a style="<%= request.getAttribute("deleteVisibility") %>" href="AdminServlet">Admin page</a></p>
	<div class="lii" style="margin-top: 20px;">Manufacturer:</div><select id="manufacturer_select" onChange="manufacturerChanged(this)">
		<option value="-2">All</option>
	<%	List<Manufacturer> manufacturers = (List<Manufacturer>)request.getAttribute("manufacturer");
		if (manufacturers != null) { 
			for (Manufacturer manufacturer : manufacturers) { %>
  				<option value="<%= manufacturer.getManufacturer_id()%>"><%= manufacturer.getName()%></option>
  			<% } %>
  		<% } %>
  	</select>
  	<br>
	<div class="lii">Model:</div>
		<% if (manufacturers != null) { 
			for (Manufacturer manufacturer : manufacturers) { %>
				<select id="<%= manufacturer.getManufacturer_id()%>" style="display: none"  > 
					<option value="-2">All</option>
				<% for (Model model: manufacturer.getModels()) {	
		%>
  					<option value="<%= model.getModelID()%>"><%= model.getName()%></option>
  				<% } %>
  				</select>
  			<% } %>
  		<% } %>
  	<br>
  	<div class="lii">Car options:</div><form id="easy_test">
	<% List<CarOption> options = (List<CarOption>)request.getAttribute("options");
		if (options != null) { 
			for (CarOption option : options) { %>
				<input type="checkbox" name="car_options" value="<%= option.getOptionID()%>"><%= option.getName()%><br>

			<% } %>
		<% } %>
		<br>
		Price from:<input type="number" name="price_start"> to: <input type="number" name="price_end"> 
	</form>
  	<br>
	<p>Sort by:</p>	
	<select id="sort_by">
  		<option value="price">Price</option>
  		<option value="date">Date</option>
	</select>
	
	<select class="lii" id="sort_order">
  		<option value="ascending">Asc</option>
  		<option value="descending">Desc</option>
	</select>
	<button  onclick="filter()">Filter</button>
	
	</div>
<br>
<br>
<br>
<br>
<tr><td>
<%				
	List<Advertisement> ads = (List<Advertisement>)request.getAttribute("ads");
	if (ads != null) {
		
		%>
		
<%
		
		for (Advertisement ad : ads) {
			String res;
			if (ad.getImage() != null) {
				res = DatatypeConverter.printBase64Binary(ad.getImage());
			} else {
				res = "iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==";
			}
			out.println("<br><table style=\"width:50%\"><tr><td colspan=\"2\"><img src=\"data:image/png;base64," + res + "\" alt=\"Red dot\"></td>" +
					"<tr><td>Description:</td><td>" + ad.getDescription() + 
					"</td></tr><tr><td>Price:</td><td>" + ad.getPrice() + 
					"</td></tr><tr><td>Date:</td><td>" + ad.getDate() +
					"</td></tr><tr><td>Model:</td><td>" + ad.getModel().getName() +
					"</td></tr><tr><td>Manufacturer:</td><td>" + ad.getManufacturer().getName() + "</td></tr><tr><td style=\"vertical-align: top;\">Car options:</td><td style=\"vertical-align: top;\">");
			int i = 0;
			%>
			
			<%
			for (CarOption option: ad.getOptions()) {
				
					out.println("" + option.getName() + "<br>");;
				i++;
			}
			%>
			</td></tr>
			<%
			out.println("<tr><td></td><td><form style=\"float: right;" + request.getAttribute("deleteVisibility") + "\" action=\"removeServlet\" method=\"get\">" +
					"<input type=\"hidden\" name=\"ad_id\" value=\"" + ad.getAdvertisementID() + "\">" +
					"<input class=\"btn-danger\" type=\"submit\" value=\"Delete\">" +
			"</form><form style=\"float: left;\" action=\"SeeAdvert\" method=\"get\">" +
			"<input type=\"hidden\" name=\"advertID\" value=\"" + ad.getAdvertisementID() + "\">" +
			"<input class=\"btn-success\" type=\"submit\" value=\"View\">" +
	"</form></td>"); 
			out.println("</tr></table>");
		
		}
%>
		<br>
		<br>
		<br>
		<br>
		<br>
		<br>	
<%
	} else {
		out.println("Error");
	}
 %>
   </td>
</body>
</html>