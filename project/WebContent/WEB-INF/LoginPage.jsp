<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<title>Insert title here</title>
</head>


<!-- Style -->
<style>

#content {
	background-color: #f0f0f4;
	border:1px solid #d3d3df;
	width: 50%;
	height: 100;
	margin: 0px 0px 0px 25%;
	padding-top: 50px;
    padding-right: 30px;
    padding-bottom: 50px;
    padding-left: 30px;
}

#buttonD {
    background-color: #3366ff;
    border: none;
    color: white;
    padding: 5px 10px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    width:50%;
    font-size: 13px;
}

select {
	width: 50%;
}

.txtVal{
	width: 50%;
}

h3 {
	color: #3366ff;
	font-size: 22px;
	margin: 0px 0px 0px 20px;
}

p {
	color: #ff3333;
}

</style>

<body>
<div id="content">
<h3> Login</h3>
<br>
<br>
<form action="login" method="post">
  	Username: <br>
  	<br>
  	<input type="text" name="username" class="txtVal"><br>
  	<br>
  	Password:<br>
  	<br>
  	 <input type="password" name="password" class="txtVal"><br>
  	<%				
	String message = (String)request.getAttribute("invalid_message");
	if (message != null) {
		out.println("<p>" + message + "</p>");
	}
	%>
	<a href="RegisterUser">Register</a>
  	<br>
  	<br>
  	<input type="submit" value="Login" id="buttonD">
</form>
</div>
</body>
</html>